var ID = {
  "header": {
    "products": "Produk",
    "why_ai": "Kenapa pilih AI?",
    "why_us": "Kenapa pilih kami?",
    "contact": "Hubungi kami",
    "try_for_free": "Coba sekarang. Gratis!"
  },
  "banner": {
    "a": {
      "title": {
        "a": "AI- Lebih hemat,",
        "b": "lebih menguntungkan",
      },
      "tip": "PERKENALKAN AI RUDDER",
      "desc": "Dengan biaya 50% solusi voicebot kami dapat meningkatkan produktivitas 1000×.",
      "btn": "Lebih lanjut"
    }
  },
  "feature": {
    "a": {
      "title": "100% Mirip Manusia",
      "content": "Teknologi sintesis bunyi kami memastikan pembicaraan pelanggan Anda senyata dengan berbicara dengan agen manusia. Sementara itu, setiap voicebot kami dirancang khusus, supaya nada, kecepatan percakapan serta sentimen voicebotnya menyerupai agen manusia dan menyesuaikan senario yang berbeda."
    },
    "b": {
      "title": "Percakapan yang Cerdas dan Handal",
      "content": "Skrip dialog out-of-box kami dilengkapi dengan pengalaman dari para ahli industri seperti telesales, keuangan dan sebagainya, itu memungkinkan voicebots kami dapat berbicara dengan cara yang persuasif, efektif, dan profesional."
    },
    "c": {
      "title": "Analisis Otomatis",
      "content": "Setelah percakapan voicebots dan pelanggan Anda, dasbor dan toolset data kami akan menawarkan visualisasi data real-time dan kecerdasan bisnis, memberdayakan keputusan manajemen yang lebih baik."
    },
    "d": {
      "title": "Integrasi yang Ringan (jika ada)",
      "content": "Solusi kami dipasangkan dengan dokumentasi yang terperinci serta API yang mudah diterapkan untuk meminimalkan pekerjaan teknik bagi pihak Anda. Untuk tim non-teknis, kami menyediakan solusi UI dan tim kesuksesan pelanggan kami untuk Anda."
    }
  },
  "product": {
    "title": "Produk Unggulan",
    "demo_btn_text": "Demo",
    "know_more_btn_text": "Lebih lanjut",
    "telemarketing_robot": {
      "name": "Robot Telemarketing",
      "desc": "Hubungi 100.000 calon pelanggan dalam 30 menit dan temukan pelanggan yang memenuhi syarat segera!"
    },
    "collection_robot": {
      "name": "Robot Koleksi",
      "desc": "5% Tingkat Pembayaran Kembali ditingkatkan"
    },
    "language": {
      "title": "Bahasa yang kami dukung",
      "bahasa": "bahasa Indonesia",
      "hindi": "Hindi",
      "flipino": "Filipina",
      "english": "Inggris"
    },
    "covered_markets": "Pasaran kami termasuk",
    "tip": "*Kami juga dapat membuat robot khusus untuk tujuan lain.",
    "btn_desc_1": "mari",
    "btn_desc_2": "kami jika Anda tertarik.",
    "btn_link": "hubungi"    
  },
  "why_ai": {
    "title": "Kenapa pilih AI?",
    "data": {
      "data_1": { 
        "strong": "90%",
        "title": "Hemat Biaya",
        "spec_1": "Perkecil tim agen Anda hingga setengahnya.",
        "spec_2": "Tanpa biaya pelatihan.",
        "spec_3": "Tanpa biaya manajemen.",
        "spec_4": "Tidak perlu ruang kantor.",
        "spec_5": "Tidak ada rencana kompensasi.",
        "spec_6": "Lebih dari 90% adalah biaya operasi."
      },
      "data_2": { 
        "strong": "1,000X",
        "title": "Efisiensi Ditingkatkan",
        "spec_1": "Tingkatkan kapasitas panggilan Anda sebanyak 1.000 kali.",
        "spec_2": "Hingga 10.000 voicebots bekerja pada waktu yang sama.",
        "spec_3": "Hingga 1.000.000 panggilan dalam sehari.",
        "spec_4": "Dalam beberapa menit saja, hasilnya sudah didapatkan !"
      },
      "data_3": { 
        "strong": "5%",
        "title": "Peningkatan Laba",
        "spec_1": "Robot koleksi: 5% Tingkat Pembayaran Kembali ditingkatkan.",
        "spec_2": "Telesales: 1000% Tingkat Kualisikasi  ditingkatkan."
      },
      "data_4": { 
        "strong": "24X7",
        "title": "Tersedia",
        "spec_1": "Performance yang stabil jalan  sepanjang hari.",
        "spec_2": "Dijadwalkan beberapa minggu sebelumnya atau dengan cepat.",
        "spec_3": "Dipicu oleh sistem upstream atau logika ."
      },
      "data_5": { 
        "title": "Data Friendly",
        "spec_1": "Pelabelan niat pelanggan yang otomatis untuk setiap panggilan.",
        "spec_2": "Metadata yang kaya untuk analisis.",
        "spec_3": "API siap untuk pengambilan data.",
        "spec_4": "Dasbor siap untuk visualisasi data."
      },
      "data_6": { 
        "title": "Peringanan Risiko",
        "spec_1": "Dialog flow dipredefinisikan dan didesain bersama.",
        "spec_2": "Performance tanpa kejutan.",
        "spec_3": "Diatur dengan sepenuhnya dan dapat dikontrol.",
        "spec_4": "Transkrip perekaman audio yang otomatis. "
      }
    },                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    "tip": "*Angka dihitung berdasarkan customer feedback.",
    "btn_desc": "Ayo, izinkan kami membantu Anda mencapai tujuan yang sama",
    "btn_link": "sekarang"
  },
  "why_us": {
    "title": "Kenapa pilih kami?",
    "data_1": {
      "title": "Yang terbaik di industri",
      "desc": "Bagi langganan kami yang ada, ketika mereka membandingkan solusi dari alternatif, solusi kami selalau menjadi pilihan yang terbaik di pasar kompetitif yang selalu berubah ini."
    },
    "data_2": {
      "title": "AI  veteran seperti kami",
      "desc": "Tim kami memiliki pengalaman percakapan dan bahasa AI selama lebih dari satu dekade, dan telah melayani ratusan juta pengguna yang berbicara dalam 250 bahasa yang berbeda."
    },
    "data_3": {
      "title": "Dipercaya oleh industri",
      "desc": "Langganan yang kami miliki lebih dari 100, dan menyebar di Asia Tenggara, India, dan bagian-bagian dunia lainnya, mencakup industri vertikal termasuk keuangan, teknologi tinggi, dan industri lainnya. Robot koleksi dan robot telemarketing kami sangat mengoptimalkan prosedur bisnis langganan kami."
    },
    "data_4": {
      "title": "Perlindungan data",
      "desc": "KKami memiliki modul perlindungan data khusus untuk menyembunyikan data sensitif dalam integrasi karena keamanan data langganan adalah perioritas utama kami. Selain itu kami juga menyediakan pergiliran nomor telepon tingkat-SIP dan tindakan keamanan lainnya."
    },
    "tip": {
      "title": "Ada pertanyaan lagi?",
      "desc": "Silahkan hubungi kami kapanpun dan kami akan merespond Anda dalam waktu 24 jam.",
      "btn": "Contact us"
    },
    "clients": "panggilan dilakukan oleh langganan global",
    "rate": "Tingkat kepuasan pelanggan",
    "try_btn": "Coba dan beruntung sekarang"
  },
  "footer": {
    "brand": "Merek-merek yang menyukai AI rudder",
    "before_leave": {
      "u": "Sebelum Anda jalankan,",
      "d": "mohon kirimkan SMS kepada kami."
    },
    "try_btn": "Coba dan beruntung dalam 5 tahun kedepan",
    "tip": "*Jejak siapkan dalam satu hari    |   *Tidak perlu pekerjaan teknik"
  },
  "connect": {
    "title": "Ayo! Hubungi kami",
    "name": "*Name",
    "email": "*Email",
    "company": "*Perusahaan",
    "whatsapp": "WhatsApp",
    "position": "Posisi",
    "phone": "Nomor HP",
    "needs": "Please describe your need.",
    "tip": "Konsultan kami akan menghubungi Anda nanti."
  }
}