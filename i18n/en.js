var EN = {
  "header": {
    "products": "Product",
    "why_ai": "Why AI?",
    "why_us": "Why us?",
    "contact": "Contact us",
    "try_for_free": "Try for free"
  },
  "banner": {
    "a": {
      "title": {
        "a": "AI - Less cost,",
        "b": "more gain",
      },
      "tip": "INTRODUCING AI RUDDER",
      "desc": "Our voicebot solution boosts 1000× productivity with 50% cost.",
      "btn": "Learn more"
    }
  },
  "feature": {
    "a": {
      "title": "100% human-like",
      "content": "Our speech synthesizing technology ensures the experience of your customer is as natural as talking to human agents. The tone, rate of speech, and sentiment of our voicebots are specially tailored to resemble human agents for different business scenarios."
    },
    "b": {
      "title": "Strong conversation",
      "content": "Our out-of-box dialog scripts are equipped with the experience from experts in industries such as telesales, finance and so forth, enabling our voicebots to talk with your customers in a persuasive, effective, and professional way."
    },
    "c": {
      "title": "Automatic analysis",
      "content": "Our data analysis toolset and dashboards offer real-time data visualization and business intelligence after the conversation between voicebots and your customers, empowering better management decisions."
    },
    "d": {
      "title": "Lightweight integration (if at all)",
      "content": "Our solutions are paired with detailed documentation as well as easy-to-implement APIs to minimize the engineering work on your side. For non-technical teams, we have you covered by our UI solution and our customer success team."
    }
  },
  "product": {
    "title": "Featured Products",
    "demo_btn_text": "Demo",
    "know_more_btn_text": "Learn more",
    "telemarketing_robot": {
      "name": "Telemarketing robot",
      "desc": "Call 100,000 prospects in 30 minutes and identify the qualified ones immediately!"
    },
    "collection_robot": {
      "name": "Collection robot",
      "desc": "Get 5% upfront repayment rate increase! "
    },
    "language": {
      "title": "Languages supported",
      "bahasa": "Indonesian",
      "hindi": "Hindi",
      "flipino": "Filipino",
      "english": "English"
    },
    "covered_markets": "Covered markets",
    "tip": "*We also do customised robots for other purposes.",
    "btn_desc_1": "Let’s have a",
    "btn_desc_2": "if you are interested.",
    "btn_link": "discussion"
  },
  "why_ai": {
    "title": "Why AI?",
    "data": {
      "data_1": { 
        "strong": "90%",
        "title": "saving on cost",
        "spec_1": "Downsize your agents team by half.",
        "spec_2": "No training cost.",
        "spec_3": "No management cost.",
        "spec_4": "No office space needed.",
        "spec_5": "No compensation plan.",
        "spec_6": "Overall 90% off on operation cost."
      },
      "data_2": { 
        "strong": "1,000X",
        "title": "efficiency boosted",
        "spec_1": "Scale up your calling capacity by 1,000 times.",
        "spec_2": "Up to 10,000 voicebots working at the same time. ",
        "spec_3": "Up to 1,000,000 calls a day.",
        "spec_4": "Get results in minutes!"
      },
      "data_3": { 
        "strong": "5%",
        "title": "increase in profit",
        "spec_1": "5% increase in repayment rate for collection robot.",
        "spec_2": "1000% increase in leads qualification rate for telesales."
      },
      "data_4": { 
        "strong": "24X7",
        "title": "available",
        "spec_1": "Stable performance throughout the day.",
        "spec_2": "Scheduled weeks before or on the fly.",
        "spec_3": "Triggered by upstream system or logic."
      },
      "data_5": { 
        "title": "Data friendly",
        "spec_1": "Automatic customer intention labeling for every call.",
        "spec_2": "Rich metadata for analysis.",
        "spec_3": "API ready for data retrieval.",
        "spec_4": "Dashboard ready for data visualization."
      },
      "data_6": { 
        "title": "Risk mitigation",
        "spec_1": "Co-design predefined dialog flow.",
        "spec_2": "No surprise in performance.",
        "spec_3": "Fully regulated and controllable.",
        "spec_4": "Automatic audio recording transcripting."
      }
    },
    "tip": "*Figures calculated based on the customer feedback.",
    "btn_desc": "Let us help you achieve the same",
    "btn_link": "Now",
  },
  "why_us": {
    "title": "Why us?",
    "data_1": {
      "title": "Best in the market",
      "desc": "In the ever-changing competitive market, our solution has always been the best choice made by our existing clients, when they compare solutions from alternatives."
    },
    "data_2": {
      "title": "AI veteran as we are",
      "desc": "Our team has the experience of speech and language AI for over a decade, having served hundreds of millions of users speaking in 250 different languages."
    },
    "data_3": {
      "title": "Trusted by industries",
      "desc": "We have over 100 clients in Southeast Asia, India and other major parts of the world, covering verticals including finance, high-tech, and other industries. Our collection and telemarketing robots greatly optimize business procedures of our clients."
    },
    "data_4": {
      "title": "Data protection",
      "desc": "To ensure our clients' data security our top priority, we have dedicated data-protection module for masking sensitive data in integration. In addition, we also provide SIP-level phone number rotation and other security measures."
    },
    "tip": {
      "title": "Any questions?",
      "desc": "Contact us at anytime. We respond within 24 hours.",
      "btn": "Contact us"
    },
    "clients": "Calls made by clients globally",
    "rate": "Customer satisfaction rate",
    "try_btn": "Try and benefit now"
  },
  "footer": {
    "brand": "Brands that love AI rudder",
    "before_leave": {
      "u": "Before you go,",
      "d": "drop us a message."
    },
    "try_btn": "Try and benefit in the next 5 years",
    "tip": "*Trial ready in one day   |   *No engineering work required"
  },
  "connect": {
    "title": "Let's connect.",
    "name": "*Name",
    "email": "*Email",
    "company": "*Company",
    "position": "Position",
    "whatsapp": "WhatsApp",
    "phone": "Phone number",
    "needs": "Please describe your need.",
    "tip": "Our consultant will get back to you soon."
  }
}