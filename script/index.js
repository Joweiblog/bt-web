$(function () {

  var IS_MOBILE = Boolean(/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent))

  var DOMAIN = '//medusa.airudder.com'
  var SUBMIT_API = DOMAIN + '/api/gauss/customers'

  var i18nextInstance;
  var loading = false
  var $_name = $('#connect-name')
  var $_email = $('#connect-email')
  var $_company = $('#connect-company')
  var $_phone = $('#connect-phone')
  var $_position = $('#connect-position')
  var $_whatsapp = $('#connect-whatsapp')
  var $_needs = $('#connect-needs')
  var $_warning_msg_box = $('#connect_err_msg')
  var $connect_btn = $('#connect_submit')
  var $connect_close = $('#connect_close')
  var $connect_modal = $('#connect_modal')
  var $connect_succ_modal = $('#success_connect_modal')
  var $count = $('#air_why_us_count')

  initLng()
  initSwiper()
  // initCount() // 暂不需要动态

  // bind
  $('#lng_en').on('click', function() { changeLng('en') })
  $('#lng_id').on('click', function() { changeLng('id') })
  $('#prod_telemarketing_robot_modal').on('hide.bs.modal', function () {
    resetVideo($('#prod_telemarketing_robot_video').get(0))
  })
  $('#prod_telemarketing_robot_modal').on('show.bs.modal', function(){
    centerVideoModal($(this))
  });
  $('#prod_collection_robot_modal').on('hide.bs.modal', function () {
    resetVideo($('#prod_collection_robot_video').get(0))
  })
  $('#prod_collection_robot_modal').on('show.bs.modal', function(){
    centerVideoModal($(this))
  });
  $('.fc-box-text>.item').on('click', function () {
    $(this).toggleClass('show-content')
  })
  $connect_btn.on('click', processConnect)
  $connect_close.on('click', processConnectHide)
  $connect_modal.on('hide.bs.modal', function () {
    resetForm()
  })

  if (IS_MOBILE) {
    $(".navbar-nav .air-header-link a").on("click",function () {
      $(".navbar-toggle").click();
    });
  }


  /**
   * center video modal
  */
  function centerVideoModal ($this) {
    var $modal_dialog = $this.find('.modal-dialog');
    $this.css('display', 'block');
    $modal_dialog.css({'margin-top': Math.max(0, ($(window).height() - $modal_dialog.height()) / 2) });
  }

  /**
   * reset video
  */
  function resetVideo (video) {
    video.currentTime = 0;
    video.pause()
  }

  /**
   * init lang
   */
  function initLng () {
    i18nextInstance = i18next.createInstance({
      lowerCaseLng: true,
      cleanCode: true,
      fallbackLng: 'en',
      resources: {
        en: {
          translation: EN
        },
        id: {
          translation: ID
        }
      },
      debug: false
    }, (err, t) => {
      if (err) return console.log('something went wrong loading', err);
    })

    jqueryI18next.init(i18nextInstance, $, {
      tName: 't',
      i18nName: 'i18n',
      handleName: 'localize',
      selectorAttr: 'data-i18n',
      targetAttr: 'i18n-target',
      optionsAttr: 'i18n-options',
      useOptionsAttr: false,
      parseDefaultValueFromContent: true
    });

    $(document).localize();
  }

  /**
   * change lang
   */
  function changeLng (lng) {
    i18nextInstance.changeLanguage(lng, function (err, t) {
      if (err) return console.log('something went wrong loading', err);
      $(document).localize();
    })
  }

  /**
   * init swiper
   */
  function initSwiper () {
    var bannerSwiper = new Swiper ('.air-banner-swiper', {
      watchOverflow: true, // 仅有1个slide，开启
      autoplay: true,
      effect: 'fade',
      // autoHeight: true, // 仅有1个slide，关闭
      pagination: {
        el: '.swiper-pagination',
      },
    })  

    var brandSwiper = new Swiper ('.air-brand-swiper', {
      pagination: {
        el: '.air-brand-swiper-pagination',
      },
    })  

    var whyAiSwiper = new Swiper ('.air-why-ai-swiper', {
      slidesPerView: 1.2,
      spaceBetween: 16,
      autoHeight: true,
      slidesOffsetBefore: 16,
      slidesOffsetAfter: 16,
      pagination: {
        el: '.air-why-ai-swiper-pagination',
      },
    })  

    var featureSwiper = new Swiper ('.air-feature-swiper', {
      slidesPerView: 1.2,
      spaceBetween: 16,
      autoHeight: true,
      slidesOffsetBefore: 16,
      slidesOffsetAfter: 16,
      pagination: {
        el: '.air-feature-swiper-pagination',
      },
    })  
    
  }

  /**
   * init count
   */
  function initCount () {
    var numAnim = new countUp.CountUp($count.get(0), 20000000, {
      startVal: 0,
      duration: 4,
    });
    numAnim.start()
    // numAnim.update(0);
  }

  /**
   * processConnectHide
  */
  function processConnectHide () {
    resetForm()
    $connect_modal.modal('hide')
  }

  /**
   * processConnect 
   */
  function processConnect () {
    if (loading) return

    var _warn_msg = ''
    $_warning_msg_box.hide()
    if (!$_name.val() || !$_email.val() || !$_company.val()) {
      _warn_msg = 'Please share with us the necessary information indicated by *.'
    }

    if (_warn_msg) {
      $_warning_msg_box.find('span').text(_warn_msg)
      $_warning_msg_box.show()
      return
    }

    var _params = {
      name: $_name.val(),
      email: $_email.val(),
      company: $_company.val(),
      phone: $_phone.val(),
      position: $_position.val(),
      whatsapp: $_whatsapp.val(),
      needs: $_needs.val()
    }

    $connect_btn.text('...')
    loading = true
    $.ajax({
      type: 'POST',
      url: SUBMIT_API,
      contentType: 'application/json',
      data: JSON.stringify(_params),
      success: function (data, status, xhr) {
        $connect_succ_modal.show()
        setTimeout(() => {
          $connect_succ_modal.hide()
        }, 2000)
        processConnectHide()
      },
      error: function (xhr, errorType, error) {
          alert('Failed: ' + JSON.stringify(error))
      },
      complete: function (xhr, status) {
          loading = false
          $connect_btn.text('SEND DETAILS')
      }
    });
  }

  function resetForm () {
    $_name.val('')
    $_email.val('')
    $_company.val('')
    $_phone.val('')
    $_position.val('')
    $_whatsapp.val('')
    $_needs.val('')
    $_warning_msg_box.hide()
  }

})